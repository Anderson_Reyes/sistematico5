﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Notes
{
    public partial class Note : Form
    {
        public Note()
        {
            InitializeComponent();
        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.ShowDialog();

           
        }

        private void guardarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(txtNote.TextLength == 0)
            {
                MessageBox.Show(null, "NO HAY NADA QUE GUARDAR", "ERROR", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            else
            {

                SaveFileDialog sfd = new SaveFileDialog();
             
                var result = sfd.ShowDialog();

                if (result == DialogResult.OK)
                {
                    String s = txtNote.Text;
                   
                    SecuencialStream ss = new SecuencialStream(sfd.FileName);
                    ss.Write(txtNote.Text);
                }
            }
        }

        private void txtNote_TextChanged(object sender, EventArgs e)
        {
            string token = txtNote.Text;
            nlines.Text = Convert.ToString(txtNote.Lines.Length);
            
        }
    }
}
