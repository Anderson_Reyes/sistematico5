﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notes
{
    class SecuencialStream
    {

        private string path;

        public SecuencialStream(string path)
        {
            this.path = path;
        }

        public void Write(string text)
        {

            try {
                using (StreamWriter sw = new StreamWriter(path)) {
                    sw.Write(text);

                }
            }
            catch (IOException) { }
        }

        private string Read()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        text += line;
                    }
                }
            }
            catch (IOException) { }
            return text;
        }


    }
}
