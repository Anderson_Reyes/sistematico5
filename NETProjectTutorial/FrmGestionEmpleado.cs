﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionEmpleado : Form
    {
        private DataSet dsEmpleados;
        private BindingSource bsEmpleados;

        public DataSet DsEmpleados
        {
           
            set
            {
                dsEmpleados = value;
            }
        }

        public FrmGestionEmpleado()
        {
            InitializeComponent();
            bsEmpleados = new BindingSource();
        }

        private void FrmGestionEmpleado_Load(object sender, EventArgs e)
        {
            bsEmpleados.DataSource = dsEmpleados;
            bsEmpleados.DataMember = dsEmpleados.Tables["Empleado"].TableName;
            dgvEmpleado.DataSource = bsEmpleados;
            dgvEmpleado.AutoGenerateColumns = true;

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FrmEmpleado fe = new FrmEmpleado();
            fe.TblEmpleados = dsEmpleados.Tables["Empleado"];
           fe.DsEmpleados= dsEmpleados;
            fe.ShowDialog();

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {

        }

        private void txtFinder_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
