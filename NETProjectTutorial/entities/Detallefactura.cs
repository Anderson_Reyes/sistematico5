﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Detallefactura
    {

        private int id;
        private Factura factura;
        private Producto producto;
        private int cantidad;
        private double precio;

        public Detallefactura(int id, Factura factura, Producto producto, int cantidad, double precio)
        {
            this.id = id;
            this.factura = factura;
            this.producto = producto;
            this.cantidad = cantidad;
            this.precio = precio;
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        internal Factura Factura
        {
            get
            {
                return factura;
            }

            set
            {
                factura = value;
            }
        }

        internal Producto Producto
        {
            get
            {
                return producto;
            }

            set
            {
                producto = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }
    }
}
