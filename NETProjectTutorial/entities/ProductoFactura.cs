﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class ProductoFactura
    {
        private int id, cantidad;
        private double precio;
        private string sku, nombre;

        public ProductoFactura(int id, int cantidad, double precio, string sku, string nombre)
        {
            this.Id = id;
            this.Cantidad = cantidad;
            this.Precio = precio;
            this.Sku = sku;
            this.Nombre = nombre;
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public string Sku
        {
            get
            {
                return sku;
            }

            set
            {
                sku = value;
            }
        }
    }
}
