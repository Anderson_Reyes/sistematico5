﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class ReporteFactura
    {
        //Factura
        private string cod_factura;
        private DateTime fecha;
        private double subtotal, iva, total;

        //DetaklleFactura
        private int cantidad;
        private double precio;

        //Producto
        private string sku, nombre_producto;

        //Empleado
        private string nombre_empleado, apellido_empleado;


        public ReporteFactura() { }
        public string Cod_factura
        {
            get
            {
                return cod_factura;
            }

            set
            {
                cod_factura = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        public double Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public double Iva
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public string Sku
        {
            get
            {
                return sku;
            }

            set
            {
                sku = value;
            }
        }

        public string Nombre_producto
        {
            get
            {
                return nombre_producto;
            }

            set
            {
                nombre_producto = value;
            }
        }

        public string Nombre_empleado
        {
            get
            {
                return nombre_empleado;
            }

            set
            {
                nombre_empleado = value;
            }
        }

        public string Apellido_empleado
        {
            get
            {
                return apellido_empleado;
            }

            set
            {
                apellido_empleado = value;
            }
        }

        public ReporteFactura(string cod_factura, DateTime fecha, double subtotal, double iva, double total, int cantidad, double precio, string sku, string nombre_producto, string nombre_empleado, string apellido_empleado)
        {
            this.Cod_factura = cod_factura;
            this.Fecha = fecha;
            this.Subtotal = subtotal;
            this.Iva = iva;
            this.Total = total;
            this.Cantidad = cantidad;
            this.Precio = precio;
            this.Sku = sku;
            this.Nombre_producto = nombre_producto;
            this.Nombre_empleado = nombre_empleado;
            this.Apellido_empleado = apellido_empleado;
        }
    }
}
