﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {
        private DataTable tblClientes;
        private DataSet dsClientes;
        private BindingSource bsClientes;
        private DataRow drClientes;

        public DataTable TblClientes
        {

            set
            {
                tblClientes = value;
            }
        }

        public DataSet DsClientes
        {

            set
            {
                dsClientes = value;
                tblClientes = dsClientes.Tables["Cliente"];
            }
        }

        public DataRow DrClientes
        {

            set
            {
                drClientes = value;
            }
        }

        public FrmCliente()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
        }

        public DataRow DrCliente
        {
            set
            {
                drClientes = value;
                txtxced.Text = drClientes["Cedula"].ToString();
                txtnombre.Text = drClientes["Nombre"].ToString();
                txtapellido.Text = drClientes["Apellido"].ToString();
                txtdir.Text = drClientes["Direccion"].ToString();
                txttel.Text = drClientes["Telefono"].ToString();
                txtcorreo.Text = drClientes["Correo"].ToString();
                

            }

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string correo, cedula, nombre, apellido, direccion
                , tel;
           

            correo = txtcorreo.Text;
            tel = txttel.Text;
            nombre = txtnombre.Text;
            apellido = txtapellido.Text;
            direccion = txtdir.Text;
           
            cedula =txtxced.Text;
            
         

            if (drClientes != null)
            {
                DataRow drNew = tblClientes.NewRow();

                int index = tblClientes.Rows.IndexOf(drClientes);
               
                drNew["Cedula"] = cedula;
                drNew["Nombre"] = nombre;
                drNew["Apellido"] = apellido;
                drNew["Direccion"] = direccion;
                drNew["Telefono"] = tel;
                


                tblClientes.Rows.RemoveAt(index);
                tblClientes.Rows.InsertAt(drNew, index);

            }
            else
            {
                tblClientes.Rows.Add(tblClientes.Rows.Count + 1
                    , cedula, nombre, apellido, direccion);
            }

            Dispose();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
