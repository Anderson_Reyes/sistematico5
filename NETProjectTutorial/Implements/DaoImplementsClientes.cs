﻿using NETProjectTutorial.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;
using System.IO;

namespace NETProjectTutorial.Implements
{
    class DaoImplementsClientes : DaoCliente
    {

        //header
        private BinaryReader brhCliente;
        private BinaryWriter bwhCliente;
        //data cliente
        private BinaryReader brdCliente;
        private BinaryWriter bwdCliente;

        private FileStream fshCliente;
        private FileStream fsdCliente;

        private const string FILENAME_HEADER = "hcliente.dat";
        private const string FILENAME_DATA = "dcliente.dat";
        private const int size = 390;

        public  DaoImplementsClientes() { }

        public void Open()
        {
            try {
                fsdCliente = new FileStream(FILENAME_DATA, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (!File.Exists(FILENAME_HEADER))
                {
                    fshCliente = new FileStream(FILENAME_HEADER, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhCliente = new BinaryReader(fshCliente);
                    bwhCliente = new BinaryWriter(fshCliente);
                    brdCliente = new BinaryReader(fsdCliente);
                    bwdCliente = new BinaryWriter(fsdCliente);
                    bwhCliente.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhCliente.Write(0);//n
                    bwhCliente.Write(0);//k
                }
                else
                {
                    fshCliente = new FileStream(FILENAME_HEADER, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhCliente = new BinaryReader(fshCliente);
                    bwhCliente = new BinaryWriter(fshCliente);
                    brdCliente = new BinaryReader(fsdCliente);
                    bwdCliente = new BinaryWriter(fsdCliente);
                }
            }
            catch (IOException) { }
        }


        public void Save(Cliente t)
        {
            Open();
            brhCliente.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhCliente.ReadInt32();
            int k = brhCliente.ReadInt32();

            long dpos = k * size;
            bwdCliente.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdCliente.Write(++k);
            bwdCliente.Write(t.Cedula);
            bwdCliente.Write(t.Nombres);
            bwdCliente.Write(t.Apellidos);
            bwdCliente.Write(t.Telefono);
            bwdCliente.Write(t.Correo);
            bwdCliente.Write(t.Direccion);

            bwhCliente.BaseStream.Seek(0, SeekOrigin.Begin);
            bwhCliente.Write(++n);
            bwhCliente.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwhCliente.BaseStream.Seek(0, SeekOrigin.Begin);
            bwhCliente.Write(k);
            Close();
        }

        
        private void Close()
        {
            try {
                if (brdCliente != null)
                {
                    brdCliente.Close();
                }
                if (brhCliente != null) {
                    brhCliente.Close();
                }
                if (bwdCliente != null)
                {
                    bwdCliente.Close();
                }
                if (bwhCliente != null)
                {
                    bwhCliente.Close();
                }
                if (fsdCliente != null)
                {
                    fsdCliente.Close();
                }
                if (fshCliente != null)
                {
                    fshCliente.Close();
                }
            }
            catch (IOException) { }
        }
        public bool delete(Cliente t)
        {
            throw new NotImplementedException();
        }

        public List<Cliente> findAll()
        {
            Open();
            List<Cliente> clientes = new List<Cliente>();

            brhCliente.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhCliente.ReadInt32();

            for(int i = 0; i < n; i++)
            {
                //calculamos posicion de la cabecera
                long hpos = 8 + i * 4;
                brhCliente.BaseStream.Seek(0, SeekOrigin.Begin);
                int index = brhCliente.ReadInt32();
                //calculamos posicion de los datos
                long dpos = (index - 1) * size;
                brdCliente.BaseStream.Seek(dpos, SeekOrigin.Begin);

                Cliente cliente = new Cliente();

                int id = brdCliente.ReadInt32();
                string cedula = brdCliente.ReadString();
                string nombre = brdCliente.ReadString();
                string apellido = brdCliente.ReadString();
                string telf = brdCliente.ReadString();
                string correo = brdCliente.ReadString();
                string direccion = brdCliente.ReadString();

                cliente = new Cliente(id, cedula, nombre, apellido, telf, correo, direccion);

                clientes.Add(cliente);
            }
            Close();

            return clientes;
        }

        public Cliente findByCedula(string cedula)
        {
            throw new NotImplementedException();
        }

        public Cliente findById(int id)
        {
            throw new NotImplementedException();
        }

        public List<Cliente> findByLastName(string Lastname)
        {
            throw new NotImplementedException();
        }

        public void save(Cliente t)
        {
            throw new NotImplementedException();
        }

        public int update(Cliente t)
        {
            throw new NotImplementedException();
        }
    }
}
