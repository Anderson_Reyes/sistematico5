﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmFactura : Form
    {
        private DataSet dsSistemas;
        private BindingSource bsProductoFactura;
        private string cod_factura, subtotal, total, iva;
        public DataSet DsSistemas
        {
            
            set
            {
                dsSistemas = value;
            }
        }

        public BindingSource BsProductoFactura
        {
            set
            {
                bsProductoFactura = value;
            }
        }

        public FrmFactura()
        {
            InitializeComponent();
            BsProductoFactura = new BindingSource();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void FrmFactura_Load(object sender, EventArgs e)
        {
            cmbEmpleados.DataSource = dsSistemas.Tables["Empleado"];
            cmbEmpleados.DisplayMember = "NA";
            cmbEmpleados.ValueMember = "Id";

            cmbProducto.DataSource = dsSistemas.Tables["Producto"];
            cmbProducto.DisplayMember = "SKUN";
            cmbProducto.ValueMember = "Id";

            bsProductoFactura.DataSource = dsSistemas.Tables["ProductoFactura"];
            dgvProductoFactura.DataSource = bsProductoFactura;

            cod_factura = "FA" + dsSistemas.Tables["Factura"].Rows.Count + 1;
            txtNoFactura.Text = cod_factura;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection dgrProductoFactura = dgvProductoFactura.SelectedRows;

            if (dgrProductoFactura.Count == 0)
            {
                MessageBox.Show(this, "ERROR, no hay filas para eliminar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataRow drProductoFactura = ((DataRowView)dgrProductoFactura[0].DataBoundItem).Row;
            dsSistemas.Tables["ProductoFactura"].Rows.Remove(drProductoFactura);
            CalcularFactura();
        }

        private void cmbProducto_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow drProductos = ((DataRowView)cmbProducto.SelectedItem).Row;
            txtCantidad.Text = drProductos["Cantidad"].ToString();
            txtPrecio.Text = drProductos["Precio"].ToString();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {


                DataRow drProducto = ((DataRowView)cmbProducto.SelectedItem).Row;
                DataRow drProductoFactura = dsSistemas.Tables["ProductoFactura"].NewRow();
                drProductoFactura["Id"] = drProducto["Id"];
                drProductoFactura["SKU"] = drProducto["SKU"];
                drProductoFactura["Nombre"] = drProducto["Nombre"];
                drProductoFactura["Cantidad"] = 1;
                drProductoFactura["Precio"] = drProducto["Precio"];
                dsSistemas.Tables["ProductoFactura"].Rows.Add(drProductoFactura);
            }
            catch (ConstraintException)
            {
                MessageBox.Show(this, "ERROR, Producto ya agregado, por favor, verifique",
                    "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvProductoFactura_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow dgrProductoFactura = dgvProductoFactura.Rows[e.RowIndex];
            DataRow drProductoFactura = ((DataRowView)dgrProductoFactura.DataBoundItem).Row;

            DataRow drProducto = dsSistemas.Tables["Producto"].Rows.Find(drProductoFactura["Id"]);

            if (Int32.Parse(drProductoFactura["Cantidad"].ToString()) > 
                Int32.Parse(drProducto["Cantidad"].ToString()))
            {
                MessageBox.Show(this, "ERROR, la cantidad a vender no puede ser mayor a la cantidad en Stock",
                    "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                drProductoFactura["Cantidad"] = Int32.Parse(drProducto["Cantidad"].ToString());
            }
            CalcularFactura();
        }

        public void CalcularFactura()
        {
            double sub = 0;
            foreach (DataGridViewRow prod in dgvProductoFactura.Rows)
            {
                 sub = Convert.ToDouble(prod.Cells["Cantidad"].Value) *
                 Convert.ToDouble(prod.Cells["Precio"].Value);
          
            }
            txtSub.Text = Convert.ToString(sub);
            txtIva.Text = Convert.ToString(sub*0.15);
            txtTotal.Text = Convert.ToString(sub*1.15);
        }

        private void dgvProductoFactura_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            CalcularFactura();
        }


        private void UpdateData()
        {
            if (cmbEmpleados.Text.Length > 1)
            {

                List<Empleado> searchData = dsSistemas.Tables["Empleado"].AsEnumerable().Select(
                    dataRow =>
                    new Empleado
                    {
                        Id = dataRow.Field<Int32>("Id"),
                        Nombres = dataRow.Field<String>("Nombres"),
                        Apellidos = dataRow.Field<String>("Apellidos")
                    }).ToList();

                HandleTextChanged(searchData.FindAll(e => e.Nombres.Contains(cmbEmpleados.Text)));
            }
            else
            {
                RestartTimer();
            }
        }

        private bool _canUpdate = true;
        private bool _needUpdate = false;


        //Actualizar el combo con nuevos datos
        private void HandleTextChanged(List<Empleado> dataSource)
        {
            var text = cmbEmpleados.Text;

            if (dataSource.Count() > 0)
            {
                cmbEmpleados.DataSource = dataSource;

                var sText = cmbEmpleados.Items[0].ToString();
                cmbEmpleados.SelectionStart = text.Length;
                cmbEmpleados.SelectionLength = sText.Length - text.Length;
                cmbEmpleados.DroppedDown = true;
                return;
            }
            else
            {
                cmbEmpleados.DroppedDown = false;
                cmbEmpleados.SelectionStart = text.Length;
            }
        }

        private void RestartTimer()
        {
            timer1.Stop();
            _canUpdate = false;
            timer1.Start();
        }

        private void cmbEmpleado_TextUpdate(object sender, EventArgs e)
        {
            _needUpdate = true;
        }

        private void cmbEmpleado_SelectedIndexChanged(object sender, EventArgs e)
        {
            _needUpdate = false;
        }

        private void cmbEmpleado_TextChanged(object sender, EventArgs e)
        {
            if (_needUpdate)
            {
                if (_canUpdate)
                {
                    _canUpdate = false;
                    UpdateData();
                }
                else
                {
                    RestartTimer();
                }
            }


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            _canUpdate = true;
            timer1.Stop();
            UpdateData();

        }

        private void btnFacturar_Click(object sender, EventArgs e)
        {
            if (dsSistemas.Tables["ProductoFactura"].Rows.Count == 0)
            {
                MessageBox.Show(this,
                    "ERROR, No se puede generar la Factura, revise que hayan productos",
                    "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataRow drFactura = dsSistemas.Tables["Factura"].NewRow();
            drFactura["Codigo"] = cod_factura;
            drFactura["Fecha"] = DateTime.Now;
            drFactura["Observaciones"] = txtObserv.Text;
            drFactura["Empleado"] = cmbEmpleados.SelectedValue;
            drFactura["Subtotal"] = subtotal;
            drFactura["Iva"] = iva;
            drFactura["Total"] = total;

            dsSistemas.Tables["Factura"].Rows.Add(drFactura);


            foreach (DataRow dr in dsSistemas.Tables["ProductoFactura"].Rows)
            {
                DataRow drDetalleFactura = dsSistemas.Tables["DetalleFactura"].NewRow();
                drDetalleFactura["Factura"] = drFactura["Id"];
                drDetalleFactura["Producto"] = dr["Id"];
                drDetalleFactura["Cantidad"] = dr["Cantidad"];
                drDetalleFactura["Precio"] = dr["Precio"];
                dsSistemas.Tables["DetalleFactura"].Rows.Add(drDetalleFactura);

                DataRow drProducto = dsSistemas.Tables["Producto"].Rows.Find(dr["Id"]);
                drProducto["Cantidad"] = Double.Parse(drProducto["Cantidad"].ToString()) -
                    Double.Parse(dr["Cantidad"].ToString());
            }

            FrmReporte frf = new FrmReporte();
            frf.MdiParent = this.MdiParent;
            frf.DsSistema = dsSistemas;
            frf.Show();

            Dispose();
        }
    }
}
