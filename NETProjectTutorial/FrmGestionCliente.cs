﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionCliente : Form
    {

        private DataSet dsClientes;
        private BindingSource bsClientes;

        public DataSet DsClientes
        {

            set
            {
                dsClientes = value;
            }
        }
        public FrmGestionCliente()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
        }

        private void FrmGestionCliente_Load(object sender, EventArgs e)
        {
            bsClientes.DataSource = dsClientes;
            bsClientes.DataMember = dsClientes.Tables["Cliente"].TableName;
            dgvClientes.DataSource = bsClientes;
            dgvClientes.AutoGenerateColumns = true;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvClientes.SelectedCells.Count==0)
            {
                MessageBox.Show(this,
                        "ERROR, Seleccione y de doble click al Cliente a editar",
                        "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this,
                        "CONFIRMACION, De doble click al Cliente a editar",
                        "Mensaje de confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void dgvCliente_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow dgrCliente = dgvClientes.Rows[e.RowIndex];
            DataRow drClienteObt = ((DataRowView)dgrCliente.DataBoundItem).Row;

            DataRow drCliente = dsClientes.Tables["Cliente"].Rows.Find(drClienteObt["Id"]);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection dgrClientes = dgvClientes.SelectedRows;

            if (dgrClientes.Count == 0)
            {
                MessageBox.Show(this, "ERROR, no hay filas para eliminar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataRow drCliente = ((DataRowView)dgrClientes[0].DataBoundItem).Row;
            dsClientes.Tables["Cliente"].Rows.Remove(drCliente);
          
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmCliente fc = new FrmCliente();
            fc.TblClientes = dsClientes.Tables["Cliente"];
            fc.DsClientes = dsClientes;
            fc.ShowDialog();
        }
    }
}
