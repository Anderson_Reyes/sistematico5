﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmReporte : Form
    {
        public FrmReporte()
        {
            InitializeComponent();
        }

        private DataSet dsSistema;

        public DataSet DsSistema
        {
           set
            {
                dsSistema = value;
            }
        }

        private void FrmReporte_Load(object sender, EventArgs e)
        {

            DataTable dtFactura = dsSistema.Tables["Factura"];
            int countFactura = dtFactura.Rows.Count;

            DataRow drFactura = dtFactura.Rows[countFactura - 1];
            DataRow drEmpleado = dsSistema.Tables["Empleado"].Rows.Find(drFactura["Empleado"]);

            DataTable dtDetalleFactura = dsSistema.Tables["DetalleFactura"];

            DataRow[] drDetalleFacturas =
                dtDetalleFactura.Select(String.Format("Factura:: {0}", drFactura["Id"]));

            foreach (DataRow dr in drDetalleFacturas) {
                DataRow drReporteFactura = dsSistema.Tables["ReporteFactura"].NewRow();
                DataRow drProducto = dsSistema.Tables["Producto"].Rows.Find(dr["Prodcuto"]);
                drReporteFactura["Cod_Factura"] = drFactura["CodFactura"];
                drReporteFactura["Fecha"] = drFactura["Fecha"];
                drReporteFactura["Subtotal"] = drFactura["Subtotal"];
                drReporteFactura["IVA"] = drFactura["Iva"];
                drReporteFactura["Total"] = drFactura["Total"];
                drReporteFactura["Nombre_Empleado"] = drEmpleado["Nombres"];
                drReporteFactura["Apellido_Empleado"] = drEmpleado["Apellidos"];
                drReporteFactura["SKU"] = drProducto["SKU"];
                drReporteFactura["Nombre_Producto"] = drProducto["Nombre"];
                drReporteFactura["Cantidad"] = drFactura["Cantidad"];
                drReporteFactura["Precio"] = drFactura["Precio"];

                dsSistema.Tables["ReporteFactura"].Rows.Add(drReporteFactura);
            }

            this.reportViewer1.LocalReport.ReportEmbeddedResource =
                "NETProjectTutorial.ReporteFactura.rdlc";
            ReportDataSource rds1 = new ReportDataSource("dsReporteFactura", dsSistema.Tables["ReporteFactura"]);

            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds1);

            this.reportViewer1.RefreshReport();
        }
    }
}
