﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {
        private static List<Empleado> Lstempleados= new List<Empleado>();

        public static List<Empleado> getLstEmpleado() {
            return Lstempleados;
        }

        public static void Populate()
        {
            // Empleado[] empleados = { new Empleado(1, "654678-2", "001-674534-678Y", "Pepito", "Perez",
            //   "Del arbolito dos cuadras abajo", "22679856", "23545.67", Empleado.Sexo.MASCULINO, 2342342),
            // new Empleado(2, "125678-2", "001-987654-678U", "Ana", "Conda",
            //"de la racachaca 1C al sur", "2278655", "67864.99", Empleado.Sexo.FEMENINO, 34243243),
            //new Empleado(3, "876457-8", "056-6786354-676R",
            //      "Armando", "Guerra",
            // "de las delicias del volga 2C al norte", "22134213", "23545.67", Empleado.Sexo.MASCULINO, 3453543)


            Lstempleados = JsonConvert.DeserializeObject<List<Empleado>>(System.Text.Encoding.Default.GetString(
                NETProjectTutorial.Properties.Resources.EMPLEADO_DATA));

            //};
            //  Lstempleados = empleados.ToList();
            //}

        }

    }
}
