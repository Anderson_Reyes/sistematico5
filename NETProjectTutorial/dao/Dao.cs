﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.dao
{
    interface Dao<T>
    {

        void save(T t);

        int update(T t);

        bool delete(T t);

        List<T> findAll();
    }
}
