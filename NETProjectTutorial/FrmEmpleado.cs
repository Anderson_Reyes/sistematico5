﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmEmpleado : Form
    {
 private DataTable tblEmpleados;
        private DataSet dsEmpleados;
        private BindingSource bsEmpleados;
        private DataRow drEmpleados;

        public DataTable TblEmpleados
        {
          
            set
            {
                tblEmpleados = value;
            }
        }

        public DataSet DsEmpleados
        {

            set
            {
                dsEmpleados = value;
                tblEmpleados = dsEmpleados.Tables["Empleado"];
            }
        }

        public DataRow DrEmpleados
        {
          
            set
            {
                drEmpleados = value;
            }
        }

        public FrmEmpleado()
        {
            InitializeComponent();
            bsEmpleados = new BindingSource();
            
        }

        public DataRow DrEmpleado
        {
            set
            {
                drEmpleados = value;
                mskInss.Text = drEmpleados["Inss"].ToString();
                mskCedula.Text = drEmpleados["Cedula"].ToString();
                txtNombre.Text = drEmpleados["Nombre"].ToString();
                txtApellido.Text = drEmpleados["Apellido"].ToString();
                txtDireccion.Text = drEmpleados["Direccion"].ToString();
                mskTelefono.Text = drEmpleados["Convencional"].ToString();
                mskCecular.Text = drEmpleados["Celular"].ToString();
                cmbSexo.Text = drEmpleados["Sexo"].ToString();

            }

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string inss, cedula, nombre, apellido, direccion, convencional
                , celular, sexo;
            double salario;

            inss = mskCecular.Text;
            cedula = mskCecular.Text;
            nombre = txtNombre.Text;
            apellido = txtApellido.Text;
            direccion = txtApellido.Text;
            convencional = mskTelefono.Text;
            celular = mskCecular.Text;
            sexo = cmbSexo.SelectedItem.ToString();
            bool result = Double.TryParse(mskSalario.Text, out salario);

            if (drEmpleados != null)
            {
                DataRow drNew = tblEmpleados.NewRow();

                int index = tblEmpleados.Rows.IndexOf(drEmpleados);
                drNew["Id"] = drEmpleados["Id"];
                drNew["Inss"] = inss;
                drNew["Cedula"] = cedula;
                drNew["Nombre"] = nombre;
                drNew["Apellido"] = apellido;
                drNew["Direccion"] = direccion;
                drNew["Telefono"] = convencional;
                drNew["Celular"] = celular;
                drNew["Sexo"] = sexo;
                drNew["Salario"] = salario;


                tblEmpleados.Rows.RemoveAt(index);
                tblEmpleados.Rows.InsertAt(drNew, index);

            }
            else
            {
                tblEmpleados.Rows.Add(tblEmpleados.Rows.Count + 1,
                    inss, cedula, nombre, apellido, direccion, convencional, celular,
                    sexo, salario);
            }

            Dispose();

        }

        private void FrmEmpleado_Load(object sender, EventArgs e)
        {
            bsEmpleados.DataSource = dsEmpleados;
            bsEmpleados.DataMember = dsEmpleados.Tables["Empleado"].TableName;


        }
    }
}
